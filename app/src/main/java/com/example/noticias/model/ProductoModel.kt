import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ProductoModel(@SerializedName("prod_id") @Expose val prodID: String?,
                    @SerializedName("prod_img") @Expose val prodImg: String?,
                    @SerializedName("prod_titulo") @Expose val prodTitulo: String?,
                    @SerializedName("prod_precio") @Expose val prodPrecio: String?,
                    @SerializedName("prod_desc") @Expose val prodDesc: String?)