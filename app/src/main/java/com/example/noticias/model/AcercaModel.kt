package com.example.noticias.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AcercaModel(@SerializedName("nosotros_id") @Expose val nosID: String?,
                   @SerializedName("nosotros_img") @Expose val nosImg: String?,
                   @SerializedName("nosotros_status") @Expose val nosStatus: String?,
                   @SerializedName("nosotros_desc") @Expose val nosDesc: String?)