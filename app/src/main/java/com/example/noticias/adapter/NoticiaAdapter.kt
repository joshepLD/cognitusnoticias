package com.example.noticias.adapter

import NoticiaModel
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.noticias.R
import com.example.noticias.activities.DetalNewsActivity
import com.example.noticias.activities.NoticiasListaActivity
import kotlinx.android.synthetic.main.item_noticia.view.*

class NoticiaAdapter(private val context: NoticiasListaActivity, private val noticiasList: List<NoticiaModel>) :
    RecyclerView.Adapter<NoticiaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_noticia, parent, false))
    }
    override fun getItemCount(): Int {
        return noticiasList.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val noticiaModel=noticiasList.get(position)
        holder.tituloNoticia?.text = noticiaModel.notTitulo
        val requestManager = Glide.with(context)
        val imageUri = noticiaModel.notImg
        val requestBuilder = requestManager.load(imageUri)
        requestBuilder.into(holder.fotoNoticia)
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetalNewsActivity::class.java)
            intent.putExtra("img_url", ""+noticiaModel.notImg)
            intent.putExtra("title", ""+noticiaModel.notTitulo)
            intent.putExtra("news_url", ""+noticiaModel.notUrl)
            intent.putExtra("desc", ""+noticiaModel.notDesc)
            context.startActivity(intent)
        }
        /*holder.ivVerMas.setOnClickListener {
            Toast.makeText(
                context,
                "Click en el icono",
                Toast.LENGTH_SHORT
            ).show()
        }*/
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tituloNoticia = view.tvTitulo
        val fotoNoticia = view.ivFotoNot
        //val ivVerMas = view.ivVerMas
    }
}
