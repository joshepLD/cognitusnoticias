package com.example.noticias.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.noticias.R
import com.example.noticias.activities.AcercaDe
import com.example.noticias.activities.DetalNewsActivity
import com.example.noticias.model.AcercaModel
import kotlinx.android.synthetic.main.item_nosotros.view.*
import kotlinx.android.synthetic.main.item_noticia.view.*

class AcercaAdapter(private val context: AcercaDe, private val nosotrosList: List<AcercaModel>) :
    RecyclerView.Adapter<AcercaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_nosotros, parent, false))
    }
    override fun getItemCount(): Int {
        return nosotrosList.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val nosotrosModel=nosotrosList.get(position)
        holder.tituloNoticia?.text = nosotrosModel.nosDesc
        val requestManager = Glide.with(context)
        val imageUri = nosotrosModel.nosImg
        val requestBuilder = requestManager.load(imageUri)
        requestBuilder.into(holder.fotoNoticia)

        /*holder.ivVerMas.setOnClickListener {
            Toast.makeText(
                context,
                "Click en el icono",
                Toast.LENGTH_SHORT
            ).show()
        }*/
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tituloNoticia = view.tvTituloNos
        val fotoNoticia = view.ivFotoNos
        //val ivVerMas = view.ivVerMas
    }
}