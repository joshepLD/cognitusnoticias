package com.example.noticias.adapter

import ProductoModel
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.noticias.R
import com.example.noticias.activities.DetailProduct
import com.example.noticias.activities.DetalNewsActivity
import com.example.noticias.activities.ProductosListaActivity
import kotlinx.android.synthetic.main.item_producto.view.*


class ProductoAdapter(private val context: ProductosListaActivity, private val productosList: List<ProductoModel>) :
    RecyclerView.Adapter<ProductoAdapter.ViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_producto, parent, false))
    }
    override fun getItemCount(): Int {
        return productosList.size
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val noticiaModel=productosList.get(position)
        holder.tituloProducto?.text = noticiaModel.prodTitulo
        holder.precioProducto?.text = noticiaModel.prodPrecio
        val requestManager = Glide.with(context)
        val imageUri = noticiaModel.prodImg
        val requestBuilder = requestManager.load(imageUri)
        requestBuilder.into(holder.fotoProducto)
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailProduct::class.java)
            intent.putExtra("img_url", ""+noticiaModel.prodImg)
            intent.putExtra("title", ""+noticiaModel.prodTitulo)
            intent.putExtra("precio", ""+noticiaModel.prodPrecio)
            intent.putExtra("desc", ""+noticiaModel.prodDesc)
            intent.putExtra("prodID", ""+noticiaModel.prodID)

            context.startActivity(intent)
        }
        /*holder.ivVerMas.setOnClickListener {
            Toast.makeText(
                context,
                "Click en el icono",
                Toast.LENGTH_SHORT
            ).show()
        }*/
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tituloProducto = view.tvTituloProd
        val fotoProducto = view.ivFotoProd
        val precioProducto = view.tvPrecioProd
        //val ivVerMas = view.ivVerMas
    }
}
