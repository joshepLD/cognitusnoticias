package com.example.noticias.activities

import ProductoModel
import android.annotation.SuppressLint
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.noticias.R
import com.example.noticias.adapter.ProductoAdapter
import com.example.noticias.task.ApiGetPostHelper
import com.example.noticias.utilities.Utils
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class ProductosListaActivity : AppCompatActivity() {

    lateinit var rvProductos: RecyclerView
    lateinit var pBar: ProgressBar
    //lateinit var tvMsg: TextView
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_productos)
        // maping of elements
        pBar = findViewById(R.id.pBarP)
        //tvMsg = findViewById(R.id.tvMsg)
        rvProductos = findViewById(R.id.rvProductos)

        // show backbutton and set custom title on actionbar
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Listado de Productos"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        //verify if we have internet connection
        if (Utils.veryfyAvailableNetwork(this)) {
            // Call AsyncTask for getting list from server in JSON format
            getProductos().execute()
        } else {
            Toast.makeText(
                applicationContext,
                "¡No cuenta con conexión a Internet!",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    // show backbutton on actionbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    // Inner class Async to consume WS
    @SuppressLint("StaticFieldLeak")
    inner class getProductos : AsyncTask<Void, Void, String>() {

        override fun onPreExecute() {
            // show progressbar for UI experience
            pBar.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg voids: Void): String {
            // Here is post Json api example
            val sendParams = HashMap<String, String>()
            // Send parameters and value on JSON api
            sendParams["Name"] = ""
            // send the HttpPostRequest by HttpURLConnection and receive a Results in return string
            return ApiGetPostHelper.SendParams(getString(R.string.pr_url), sendParams)
        }

        override fun onPostExecute(results: String?) {
            // Hide Progressbar
            pBar.visibility = View.GONE
            //tvMsg.visibility = View.VISIBLE

            if (results != null) {
                // See Response in Logcat for understand JSON Results
                Log.i("Resultado: ", results)
            }

            try {
                val listaProductos = ArrayList<ProductoModel>()
                // create JSONObject from string response
                val rootJsonObject = JSONObject(results)
//                val isSucess = rootJsonObject.getString("noticias")
                val productosObject = rootJsonObject.getString("productos")
                val productosArray = JSONArray(productosObject)
                for (i in 0 until productosArray.length()) {
                    // Get single JSON object node
                    val sObject = productosArray.get(i).toString()
                    val mItemObject = JSONObject(sObject)
                    // Get String value from json object
                    val prodId = mItemObject.getString("prod_id")
                    val prodImg = getString(R.string.img_url) + mItemObject.getString("prod_img")
                    val prodTitulo = mItemObject.getString("prod_titulo")
                    val prodPrecio = mItemObject.getString("prod_precio")
                    val prodDesc = mItemObject.getString("prod_desc")
                    val objeto = ProductoModel(prodId, prodImg, prodTitulo, prodPrecio, prodDesc)
                    listaProductos.add(objeto)
                }

                linearLayoutManager = LinearLayoutManager(applicationContext)
                val rvProductos = findViewById<RecyclerView>(R.id.rvProductos)

                rvProductos.layoutManager = linearLayoutManager
                rvProductos.adapter = ProductoAdapter(this@ProductosListaActivity, listaProductos)
            } catch (e: JSONException) {
                Toast.makeText(
                    applicationContext,
                    "Lo sentimos, algo salio mal. ¡Intenta de nuevo!",
                    Toast.LENGTH_SHORT
                ).show()
                e.printStackTrace()
            }
        }
    }
}
