package com.example.noticias.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.noticias.task.ApiGetPostHelper
import com.example.noticias.R
import com.example.noticias.utilities.Utils
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    lateinit var strUsr: String
    lateinit var strNip: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnIniciar.setOnClickListener {
            if (edtUsuario.text.isEmpty() || edtContraseña.text.isEmpty()) {
                Toast.makeText(this, "Debes escribir Datos", Toast.LENGTH_LONG).show()
            } else {
                    if (Utils.isEmailValid("" + edtUsuario.text)) {
                    if (Utils.veryfyAvailableNetwork(this)) {
                        val imm =
                            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                        strUsr = edtUsuario.text.toString()
                        strNip = edtContraseña.text.toString()
                        getUsuarioIngreso().execute()
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "No cuenta con conexion a internet!",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } else {
                    Toast.makeText(this, "Formato de correo no valido", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    inner class getUsuarioIngreso : AsyncTask<Void, Void, String>() {

        override fun onPreExecute() {
            pBar.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg params: Void?): String {
            val sendParams = HashMap<String,String>()
            sendParams["valor"] = ""
            return ApiGetPostHelper.SendParams(
                getString(R.string.ws_url_login) + "getLoginUsr&email=" + strUsr + "&nip=" + strNip,
                sendParams
            )
        }

        override fun onPostExecute(results: String?) {
            // Hide Progressbar
            pBar.visibility = View.GONE

            if (results != null) {
                // See Response in Logcat for understand JSON Results
                Log.i("Resultado: ", results)
            }
            try {
                // create JSONObject from string response
                val rootJsonObject = JSONObject(results)
                val validoObject = rootJsonObject.getString("valido")
                if (validoObject == "1") {
                    val usuarioObject = rootJsonObject.getString("usuario")
                    val mItemObject = JSONObject(usuarioObject)

                    val usrIdS = mItemObject.getString("usr_id")
                    val usrNombre = mItemObject.getString("usr_nombre")

                    val sharedPreference =  getSharedPreferences("mi_app_cognitus",Context.MODE_PRIVATE)
                    var editor = sharedPreference.edit()
                    editor.putString("usr_id",usrIdS)
                    editor.putString("usr_name",usrNombre)
                    editor.commit()

                    val intent = Intent(applicationContext, Home::class.java)
                    intent.putExtra("nombre_usr",  usrNombre)
                    intent.putExtra("usr_id", usrIdS)
                    startActivity(intent)
                    finish()
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Datos incorrectos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } catch (e: JSONException) {
                Toast.makeText(
                    applicationContext,
                    "Lo sentimos, algo salio mal. ¡Intenta de nuevo!",
                    Toast.LENGTH_SHORT
                ).show()
                e.printStackTrace()
            }
        }

    }
}
