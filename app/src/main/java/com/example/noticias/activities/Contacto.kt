package com.example.noticias.activities

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.noticias.R
import com.example.noticias.task.ApiGetPostHelper
import com.example.noticias.utilities.DialogoAlerta
import com.example.noticias.utilities.Utils
import kotlinx.android.synthetic.main.activity_contacto.*
import org.json.JSONException
import org.json.JSONObject

class Contacto : AppCompatActivity() {

    lateinit var strUsr: String
    lateinit var strCorr: String
    lateinit var strMen: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacto)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Contacto"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)

        }

        btnIniciarCont.setOnClickListener {
            if (edtUsuarioCont.text.isEmpty() || edtCorreoCont.text.isEmpty() || edtMensajeCont.text.isEmpty()) {
                Toast.makeText(this, "Debes escribir Datos", Toast.LENGTH_LONG).show()
            } else {
                if (Utils.isEmailValid("" + edtCorreoCont.text)) {
                    if (Utils.veryfyAvailableNetwork(this)) {
                        val imm =
                            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                        strUsr = edtUsuarioCont.text.toString()
                        strCorr = edtCorreoCont.text.toString()
                        strMen = edtMensajeCont.text.toString()
                        getContacto().execute()
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "No cuenta con conexion a internet!",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                } else {
                    Toast.makeText(this, "Formato de correo no valido", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    @SuppressLint("StaticFieldLeak")
    inner class getContacto : AsyncTask<Void, Void, String>() {

        override fun onPreExecute() {
        }

        override fun doInBackground(vararg params: Void?): String {
            val sendParams = HashMap<String,String>()
            sendParams["valor"] = ""
            return ApiGetPostHelper.SendParams(
                getString(R.string.ws_url_contact) + "&getContacto&nombre=" + strUsr + "&correo=" + strCorr + "&mensaje" + strMen ,
                sendParams
            )
        }

        override fun onPostExecute(results: String?) {
            // Hide Progressbar
            //pBar.visibility = View.GONE

            if (results != null) {
                // See Response in Logcat for understand JSON Results
                Log.i("Resultado: ", results)
            }
            try {
                // create JSONObject from string response
                val rootJsonObject = JSONObject(results)
                val validoObject = rootJsonObject.getString("valido")
                if (validoObject == "1") {
                    DialogoAlerta.crearDialogo(this@Contacto,"Atención", "Sera contactado en breve")
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Datos incorrectos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } catch (e: JSONException) {
                Toast.makeText(
                    applicationContext,
                    "Lo sentimos, algo salio mal. ¡Intenta de nuevo!",
                    Toast.LENGTH_SHORT
                ).show()
                e.printStackTrace()
            }
        }

    }
}
