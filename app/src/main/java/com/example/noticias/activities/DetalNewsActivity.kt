package com.example.noticias.activities

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.noticias.R
import kotlinx.android.synthetic.main.activity_newsdetail.*

class DetalNewsActivity: AppCompatActivity() {
    lateinit var strUrl: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newsdetail)
        var iVNew = findViewById<ImageView>(R.id.iVNew)
        var tVTitle = findViewById<TextView>(R.id.tVTitle)
        var tVContent = findViewById<TextView>(R.id.tVContent)
        val urlImg: String = intent.getStringExtra("img_url")
        val title: String = intent.getStringExtra("title")
        val conten: String = intent.getStringExtra("desc")
        val nUrl: String = intent.getStringExtra("news_url")
        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load(urlImg)
        requestBuilder.into(iVNew)
        tVTitle.setText(title)
        tVContent.setText(conten)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Detalle de noticia"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        btnCompartir.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.msj_compartir) + nUrl);
            startActivity(Intent.createChooser(shareIntent, getString(R.string.titulo_compartir)))
        }
        btnView.setOnClickListener {
            val intent = Intent(this, Webview::class.java)
            intent.putExtra("news_url", ""+nUrl)
            startActivity(intent)
        }

    }

    // show backbutton on actionbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}