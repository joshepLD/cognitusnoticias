package com.example.noticias.activities


import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.noticias.R
import com.example.noticias.task.ApiGetPostHelper
import com.example.noticias.utilities.DialogoAlerta
import com.example.noticias.utilities.Utils
import kotlinx.android.synthetic.main.activity_contacto.*
import kotlinx.android.synthetic.main.activity_detail_product.*
import org.json.JSONException
import org.json.JSONObject

class DetailProduct : AppCompatActivity() {
    lateinit var strId: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product)
        var iVNew = findViewById<ImageView>(R.id.iVProd)
        var tVTitle = findViewById<TextView>(R.id.tVTitleProd)
        var tVPrecio = findViewById<TextView>(R.id.tVPrecioProd)
        var tVContent = findViewById<TextView>(R.id.tVContentProd)
        val urlImg: String = intent.getStringExtra("img_url")
        val title: String = intent.getStringExtra("title")
        val conten: String = intent.getStringExtra("desc")
        val precio: String = intent.getStringExtra("precio")
        strId = intent.getStringExtra("prodID")
        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load(urlImg)
        requestBuilder.into(iVNew)
        tVTitle.setText(title)
        tVPrecio.setText(precio)
        tVContent.setText(conten)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Detalle de producto"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        btnCarrito.setOnClickListener{
            if (Utils.veryfyAvailableNetwork(this)) {
                getOrden().execute()
            } else {
                Toast.makeText(
                    applicationContext,
                    "No cuenta con conexion a internet!",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

    }

    @SuppressLint("StaticFieldLeak")
    inner class getOrden : AsyncTask<Void, Void, String>() {

        override fun onPreExecute() {
        }

        override fun doInBackground(vararg params: Void?): String {
            val sendParams = HashMap<String,String>()
            sendParams["valor"] = ""
            return ApiGetPostHelper.SendParams(
                getString(R.string.ws_url_Orden) + "&id_producto=" + strId,
                sendParams
            )
        }

        override fun onPostExecute(results: String?) {
            // Hide Progressbar
            //pBar.visibility = View.GONE

            if (results != null) {
                // See Response in Logcat for understand JSON Results
                Log.i("Resultado: ", results)
            }
            try {
                // create JSONObject from string response
                val rootJsonObject = JSONObject(results)
                val validoObject = rootJsonObject.getString("valido")
                if (validoObject == "1") {
                    val noticiasObject = rootJsonObject.getString("respuesta")
                    val mItemObject = JSONObject(noticiasObject)
                    val notId = mItemObject.getString("orden")
                    DialogoAlerta.crearDialogo(this@DetailProduct,"Gracias por su compra", "su numero de compra es: "+ notId)
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Datos incorrectos",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } catch (e: JSONException) {
                Toast.makeText(
                    applicationContext,
                    "Lo sentimos, algo salio mal. ¡Intenta de nuevo!",
                    Toast.LENGTH_SHORT
                ).show()
                e.printStackTrace()
            }
        }

    }

    // show backbutton on actionbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
