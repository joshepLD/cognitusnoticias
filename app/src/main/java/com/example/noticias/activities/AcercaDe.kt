package com.example.noticias.activities

import android.annotation.SuppressLint
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.noticias.R
import com.example.noticias.adapter.AcercaAdapter
import com.example.noticias.model.AcercaModel
import com.example.noticias.task.ApiGetPostHelper
import com.example.noticias.utilities.Utils
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class AcercaDe : AppCompatActivity() {
    //Declarate global variables
    lateinit var rvNoticias: RecyclerView
    lateinit var pBar: ProgressBar
    //lateinit var tvMsg: TextView
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acerca_de)
        // maping of elements
        pBar = findViewById(R.id.pBarNos)
        //tvMsg = findViewById(R.id.tvMsg)
        rvNoticias = findViewById(R.id.rvNosotros)

        // show backbutton and set custom title on actionbar
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Acerca de Nosotros"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        //verify if we have internet connection
        if (Utils.veryfyAvailableNetwork(this)) {
            // Call AsyncTask for getting list from server in JSON format
            getNosotros().execute()
        } else {
            Toast.makeText(
                applicationContext,
                "¡No cuenta con conexión a Internet!",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    // show backbutton on actionbar
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    // Inner class Async to consume WS
    @SuppressLint("StaticFieldLeak")
    inner class getNosotros : AsyncTask<Void, Void, String>() {

        override fun onPreExecute() {
            // show progressbar for UI experience
            pBar.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg voids: Void): String {
            // Here is post Json api example
            val sendParams = HashMap<String, String>()
            // Send parameters and value on JSON api
            sendParams["Name"] = ""
            // send the HttpPostRequest by HttpURLConnection and receive a Results in return string
            return ApiGetPostHelper.SendParams(getString(R.string.nos_url), sendParams)
        }

        override fun onPostExecute(results: String?) {
            // Hide Progressbar
            pBar.visibility = View.GONE
            //tvMsg.visibility = View.VISIBLE

            if (results != null) {
                // See Response in Logcat for understand JSON Results
                Log.i("Resultado: ", results)
            }

            try {
                val listaNosotros = ArrayList<AcercaModel>()
                // create JSONObject from string response
                val rootJsonObject = JSONObject(results)
//                val isSucess = rootJsonObject.getString("noticias")
                val nosotrosObject = rootJsonObject.getString("nosotros")
                val noticiasArray = JSONArray(nosotrosObject)
                for (i in 0 until noticiasArray.length()) {
                    // Get single JSON object node
                    val sObject = noticiasArray.get(i).toString()
                    val mItemObject = JSONObject(sObject)
                    // Get String value from json object
                    val nosId = mItemObject.getString("nosotros_id")
                    val nosImg = mItemObject.getString("nosotros_img")
                    val nosStatus = mItemObject.getString("nosotros_status")
                    val nosDesc = mItemObject.getString("nosotros_desc")
                    val objeto = AcercaModel(nosId, nosImg, nosStatus, nosDesc)
                    listaNosotros.add(objeto)
                }

                linearLayoutManager = LinearLayoutManager(applicationContext)
                val rvNoticias = findViewById<RecyclerView>(R.id.rvNosotros)

                rvNoticias.layoutManager = linearLayoutManager
                rvNoticias.adapter = AcercaAdapter(this@AcercaDe, listaNosotros)
            } catch (e: JSONException) {
                Toast.makeText(
                    applicationContext,
                    "Lo sentimos, algo salio mal. ¡Intenta de nuevo!",
                    Toast.LENGTH_SHORT
                ).show()
                e.printStackTrace()
            }
        }
    }
}
