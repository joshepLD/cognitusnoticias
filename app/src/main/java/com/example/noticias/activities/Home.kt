package com.example.noticias.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.noticias.R
import kotlinx.android.synthetic.main.activity_home.*

class Home : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val btnCerrarS = findViewById<Button>(R.id.btnCerrarS)

        btnCerrarS?.setOnClickListener {
            val sharedPreference =  getSharedPreferences("mi_app_cognitus", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putString("usr_id","")
            editor.commit()

            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        lnNoticiasHome.setOnClickListener{
             val noticias = Intent(this, NoticiasListaActivity::class.java)
             startActivity(noticias)
        }
        lnProductosHome.setOnClickListener{
            val productos = Intent(this, ProductosListaActivity::class.java)
            startActivity(productos)
        }
        lnAcercaHome.setOnClickListener{
            val productos = Intent(this, AcercaDe::class.java)
            startActivity(productos)
        }
        lnContactoHome.setOnClickListener{
            val productos = Intent(this, Contacto::class.java)
            startActivity(productos)
        }
    }
}
